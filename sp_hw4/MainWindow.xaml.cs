﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace sp_hw4
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            fontFamily.ItemsSource = Fonts.SystemFontFamilies.OrderBy(f => f.Source);
            fontSize.ItemsSource = new List<double>() { 8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 72 };

           var timer = new DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, 10);
            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            var textTemp = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd);
            ThreadPool.QueueUserWorkItem(AutoSave, new object[] { textTemp });
        }

        private void ExitClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void RichTextBoxSelectionChanged(object sender, RoutedEventArgs e)
        {
            object temp = richTextBox.Selection.GetPropertyValue(Inline.FontWeightProperty);
            buttonBold.IsChecked = (temp != DependencyProperty.UnsetValue) && (temp.Equals(FontWeights.Bold));
            temp = richTextBox.Selection.GetPropertyValue(Inline.FontStyleProperty);
            buttonItalic.IsChecked = (temp != DependencyProperty.UnsetValue) && (temp.Equals(FontStyles.Italic));
            temp = richTextBox.Selection.GetPropertyValue(Inline.TextDecorationsProperty);
            buttonUnderline.IsChecked = (temp != DependencyProperty.UnsetValue) && (temp.Equals(TextDecorations.Underline));

            temp = richTextBox.Selection.GetPropertyValue(Inline.FontFamilyProperty);
            fontFamily.SelectedItem = temp;
            temp = richTextBox.Selection.GetPropertyValue(Inline.FontSizeProperty);
            fontSize.Text = temp.ToString();
        }

        private void FontFamilySelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (fontFamily.SelectedItem != null)
                richTextBox.Selection.ApplyPropertyValue(Inline.FontFamilyProperty, fontFamily.SelectedItem);
        }



        private void FontSizeSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            richTextBox.Selection.ApplyPropertyValue(Inline.FontSizeProperty, fontSize.Text);
        }

        private void OpenExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Rich Text Format (*.rtf)|*.rtf|All files (*.*)|*.*";
            if (dlg.ShowDialog() == true)
            {
                FileStream fileStream = new FileStream(dlg.FileName, FileMode.Open);
                TextRange range = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd);
                range.Load(fileStream, DataFormats.Rtf);
                fileStream.Dispose();
            }
        }

        private void SaveExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Rich Text Format (*.rtf)|*.rtf|All files (*.*)|*.*";
            if (dlg.ShowDialog() == true)
            {
                FileStream fileStream = new FileStream(dlg.FileName, FileMode.Create);
                TextRange range = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd);
                range.Save(fileStream, DataFormats.Rtf);
                fileStream.Dispose();
            }
        }

        private void AutoSave(object obj)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)+"\\temp.rtf";
            object[] objArray = obj as object[];
            var tmpRange = (TextRange)objArray[0];

            using (StreamWriter writer = new StreamWriter(path))
            {
                writer.Write(tmpRange.Text);
            }
        }
    }
}
